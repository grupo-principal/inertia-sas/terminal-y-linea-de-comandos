#!/bin/bash

# 1 DIRECTORIOS
# mkdir MiDirectorio
# mkdir dir1 dir2 dir3
# touch ./dir1/file1 ./dir1/file2 ./dir1/file3
# cp  ./dir1/file1 ./dir2/file_1
# mv ./dir2/file_1 ./dir2/filecopy
# rm ./dir1/file3
# rm -i ./dir1/file2
# cp  ./dir1/file1 ./dir3/file_1
# mv dir3 dir2
# rm -ri dir2/dir3
# tree

# 2 EXPLORAR ARCHIVOS
# head principios-de-usabilidad.md
# head principios-de-usabilidad.md -n 15
# tail principios-de-usabilidad.md
# tail principios-de-usabilidad.md -n 20
# export LESSCHARSET=utf-8
# less principios-de-usabilidad.md # Sales pulsando q

# 3 COMMAND
# type cd
# type mkdir
# type ls
# alias l="ls -lh"
# help cd
# ls --help
# man cd
# info cd

# 4 WILDCARS
# touch file.txt dot.txt dot2.text Index.html datos1  datos123 abc
# ls *.txt
# ls datos*
# ls datos?
# ls datos???
# ls *.html
# ls [[:upper:]]*
# ls -d [[:upper:]]*
# ls -d [[:lower:]]*
# ls [ad]*
# ls [ai]*

# 5 REDIRECCIONES
# ls > ./archivo.txt
# ls >> ./archivo.txt
# ls fjldsjf 2> error.txt
# ls fdsdfafdda > output.txt 2>&1
# ls > output2.txt 2>&1

# 6 PIPE OPERATOR
# cat < 6_pipe.sh
# ls -lh | cat | sort | tee output.txt | less
# cowsay "hola"

# 7 OPERADORES DE CONTROL
# ls; mkdir holi; cal
# ls & cal & date
# mkdir test && cd test
# cd directorio && touch archivo.txt && touch archivo.txt && echo "Archivo"
# cd directorio || touch archivo.txt || touch archivo.txt || echo "Archivo creado"

# 8 PERMISOS
# mkdir sandbox
# cd sandbox
# > mitexto.txt
# cat > mitexto.txt
# hola amigos
# Desde INERTiA
# chmod 755 mitexto.txt # provee todos los permisos al usario
# chmod u-r mitexto.txt # elimina permiso de lectura al usuario
# chmod u+r mitexto.txt # Vuele a dar permisos de lectura al usuario
# chmod u-x,go=w mitexto.txt # quita (-) permisos de ejecución al usuario, y grupos y otros (world) sobre escribe (=) y quedan con sólo permiso de escritura
# whoami # me indica quien soy
# id # indica el uid del usuario e info de grupos a los que pertenece.
# su # (swith user) cambiar a otro usuario
# sudo # Permisos temporales como root
# passwd # Permite cambiar la contraseña

# 9 VARIABLES DE ENTORNO
# ln -s ~/terminal_app/app/ Application # Se crea un link simbólico, o acceso directo desde la terminal
# printenv # muestra todas las variables de entorno que se encuentran configuradas
# echo $HOME # imprime la variable de entorno HOME (directorio home)
# cd $HOME # nos envía al directorio home.
# echo $PATH # Nos muestra todas las rutas de los binarios que ejecuta nuestro sistema.
# .bashrc # Contiene los archivos donde se mantiene la configuración del bash
# .zshrc # Contiene los archivos donde se mantiene la configuración del zsh (por ejemplo para equpos MAC)
# code .bashrc # modificar el archivo con visual studio code

# 10. COMANDOS DE BÚSQUEDA
# which less # ayuda a encontrar la ruta de los binarios (por ejemplo /usr/bin/less para less)
# find ./ -name file # Busca un archivo, y se le indica una ruta dentro de la cual se realizará la búsqueda (por ejemplo, todos los archivos llamados file) y la manera en la que se realizará la búsqueda
# find ./ -name *.txt # Busca utilizando wildcards (todos los archivos terminados en .txt por ejemplo)
# find ./ -type fd # busca archivos (f, files) o directorios (d, directories)
# find ./ -type f -name *.log # Para encontrar archivos de log
# find ./ -size 20M # Encunetra archivos que sean mayores a 20M

# 11. GREP
# grep Towers movies.csv # encontrar todas las películas que tenga la palabra `Towers` dentro de `movies.csv`
# grep -i the movies.csv # Para ignorar case sensitive
# grep -i the movies.csv | less # Redirigiendo a `less` se facilita la lectura
# grep -c the movies.csv # Contar número de ocurrencias
# grep -ci the movies.csv # no se tiene en cuenta el case sensitive
# grep -vi tower movies.csv # Encontrar NO coincidencias
# wc movies.csv # Para contar todas la palabras en un archivo

# 12. UTILIDADES DE RED
# ifconfig # Información de la red
# ping www.google.com # Revisar si una IP está activa
# curl www.google.com # Traer archivo como texto
# wget www.google.com # Indicar la ruta hasta llegar al sitio que se está visitando
# traceroute www.google.com # Indicar la ruta hasta llegar al sitio que se está visitando
# netstat -i # Imprimir las conexiones de red, tablas de enrutado, estadísticas, etc.

# 13. COMPRIMIR ARCHIVOS
# tar -cvf toCompress.tar toCompress # crear un nuevo archivo comprimido 
# tar -cvzf toCompress.tar.gz toCompress # formato de compresion gzip
# tar -xvzf toCompress.tar.gz # descomprimir
# zip -r toCompressInZip.zip toCompress # comprimir a formato .zip
# unzip toCompressInZip.zip # Descomprimir zip

# 14. MANEJAR PROCESOS
# ps # Listar procesos de la terminal actual
# kill 33131 # mata el proceso con pid 33131
# top # lista los procesos que utilizan más recursos
# htop # vesion mejorada de top