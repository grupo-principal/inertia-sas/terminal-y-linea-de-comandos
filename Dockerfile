# FROM ubuntu:latest
FROM ubuntu:xenial

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get install tree
RUN apt-get install less
RUN bash -c 'yes | apt-get install man-db'
RUN bash -c 'yes | apt-get install info'
RUN apt-get -y install net-tools
RUN apt-get -y install iputils-ping
RUN apt-get -y install curl
RUN apt-get -y install wget
RUN apt-get -y install traceroute
RUN apt-get -y install zip
RUN apt-get -y install unzip
RUN apt-get -y install htop
RUN apt-get -y install glances
RUN apt-get -y install vim
# RUN apt-get install whatis

WORKDIR /root/terminal_app

COPY [".","."]

CMD ["tail", "-f", "/dev/null"]