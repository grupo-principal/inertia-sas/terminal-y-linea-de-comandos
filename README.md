# Terminal y Línea de Comandos
## Contenido
- [Docker](#docker)
- [Descriptores de rutas](#descriptores-de-rutas)
- [Atajos de teclado](#atajos-de-teclado)
- [Manipulando archivos y directorios](#manipulando-archivos-y-directorios)
- [Explorando el contenido de los archivos](#explorando-el-contenido-de-los-archivos)
- [Tipos de comando](#tipos-de-comando)
- [Wildcards](#wildcards)
- [Redirecciones](#redirecciones)
- [Pipe operator](#pipe-operator)
- [Operadores de control](#operadores-de-control)
- [Permisos](#permisos)
- [Modificar permisos](#modificar-permisos)
- [Variables de entorno](#variables-de-entorno)
- [Comandos de busqueda](#comandos-de-búsqueda)
- [GREP](#grep)
- [Utilidades de red](#utilidades-de-red)
- [Comprimir archivos](#comprimir-archivos)
- [Manejar procesos](#manejar-procesos)
- [Proceso en Foreground y Background](#procesos-en-foreground-y-background)
- [Editar texto con vim](editar-texto-con-vim)
- [Personalizar el terminal](editar-texto-con-vim)
- [Lecturas recomendadas](#lecturas-recomendas)
## Docker
### Inciar contenedor
```
docker-compose up --build -d
```
###
```
docker-compose exec app bash
```
## Descriptores de rutas
Ruta raíz del sistema
```
/ 
```
Directorio actual
```
.
```
Directorio anterior
```
..
```
Directorio home del usuario 
```
~
```
## Atajos de teclado
- `CTRL + C` Termina el proceso de un comando en la terminal
- `CTRL + D` Termina el input de un comando
- `CTRL + A` Avanza al inicio de línea
- `CTRL + E` Avanza al final de línea
- `CTRL + L` Limpia la pantalla de la terminal
## Manipulando archivos y directorios
### Moviendose entre directorios
Imprimir el directorio actual
```
pwd
```
Cambiar al directorio con nombre `dir1`
```
cd dir1
```
Cambia dos directorios anteriores del actual
```
cd ../..
```
Cambia al directorio home del usuario
```
cd
```
### Mostrar archivos y directorios
Muestra archivos y directorios
```
ls
```
Muestra las caracteristicas del archivo
```
file archivo.txt
``` 
#### Operaciones de ls
- `-a` Muestra todo (incluyendo archivos ocultos)
- `-R` Muestra una lista de manera recursiva
- `-r` Muestra listando de forma inversa
- `-t` Muestra los últimos modificados
- `-S` Muestra ordenando por tamaño
- `-l` Muestra usando un formato largo

### Abrir archivos
```
xdg-open archivo.txt
```

### Crear directorios
Un directorio
```
mkdir MiDirectorio
```
Varios directorios
```
mkdir dir1 dir2 dir3
```
### Crear un archivo
```
touch ./dir1/file1 ./dir1/file2 ./dir1/file3
```
### Copiar un archivo
```
cp  ./dir1/file1 ./dir2/file_1
```
### Mover un archivo
```
mv ./dir2/file_1 ./dir2/filecopy
```
### Cambiar nombre
```
mv dir3 dir2
```
### Eliminar un archivo
```
rm ./dir1/file3
```
Preguntar antes de cada borrado
```
rm -i ./dir1/file2
```
Remover recursivamente preguntado
```
rm -ri dir2/dir3
```
### Arbol de directorios y archivos
```
tree
```
## Explorando el contenido de los archivos
### Leyendo las primeras líneas
Las primeras 10 líneas
```
head principios-de-usabilidad.md
```
Las primeras 15 líneas
```
head principios-de-usabilidad.md -n 15
```
### Leyendo las últimas líneas
Las últimas 10 líneas
```
tail principios-de-usabilidad.md
```
Las últimas 20 líneas
```
tail principios-de-usabilidad.md -n 20
```
### Leyendo el archivo completo
```
less principios-de-usabilidad.md
```
Sales pulsando `q`

## Tipos de comando
Un comando puede ser:
- Un programa ejecutable.
- Un comando de utilidad de la shell.
- Una función de shell.
- Un alias.
### Identificar el tipo de comando
Función del shell
```
type cd
```
Alias
```
type ls
```
### Crear un alias
```
alias l=”ls -lh”
```
### Leer opción de ayuda de los comandos
```
cd --help
```
Manual de usuario de un comando
```
man ls
```
Descripción muy corta de lo que hace un comando
```
whatis
```
Muestra la ruta de donde se encuentra
el ejecutable del comando
```
which command
```

## Wildcards
Finaliza con `.txt`
```
ls *.txt
```
Comienza con `datos`
```
ls datos*
```
Comienza con datos y continúa un caracter 
```
ls datos?
```
Comienza con datos y continúa con 3 carateres
```
ls datos???
```
El primer caractér es mayúscula
```
ls [[:upper:]]*
```
Es un directorio cuyo primer caracter es mayúscula
```
ls -d [[:upper:]]*
```
Es un directorio cuyo primer caracter es minúscula
```
ls -d [[:lower:]]*
```
Comienza con `a` o con `d`
```
ls [ad]*
```
Comienza con `a` o con `i`
```
ls [ai]*
```
## Redirecciones
### Standar Input
```
cat < 6_pipe.sh
```
### Standar Output
Borra todo su contenido y se guarda el actual
```
ls 1> ./archivo.txt 
```
Se puede escribir de manera más simple como
```
ls > ./archivo.txt 
```
Si no se desea borrar la información previa sino anexarla a la información anterior
```
ls >> ./archivo.txt 
```
### Standar Error
```
ls fjldsjf 2> error.txt
```
### Redireccionar el error (2) si existe al output (1)
```
ls fdsdfafdda > output.txt 2>&1
```
## Pipe operator
```
ls -lh | cat | sort | tee output.txt | less
```
## Operadores de control
Se ejecuta uno seguido del otro en orden, hasta que el anterior haya terminado
```
ls; mkdir holi; cal
```
Se ejecutan todos al mismo tiempo, de forma asíncrona
```
ls & cal & date
```
Se ejecutan sólo si el comando anterior se haya ejecutado exitosamente
```
cd directorio && touch archivo.txt && echo "Archivo"
```
Sólo ejecuta el primero que se ejecute exitosamente
```
cd directorio || touch archivo.txt || echo "Archivo creado"
```

### Lectura recomendada
- [¿Cuáles son los operadores de control y redirección de shell?](https://qastack.mx/unix/159513/what-are-the-shells-control-and-redirection-operators)

## Permisos
Existen diversos usuarios con permisos cada uno; el usuario `root` es especial y puede hacer de todo.

### Consultar permisos con `ls -l`
Tipos de archivos
- `-`: archivo normal.
- `d`: directorio.
- `l`: link simbólico.
- `b`: archivo de bloque especial.

Tipos de modos:
- `r`: Read
- `w`: Write
- `x`: eXecute

#### Notación
| [tipo] |rwx|rwx|rwx|
|---|---|---|---|
|Type|Owner|Group|World|

```
<type><rwx owner><rwx group><rwx world>
```
Para un directorio
```
 drw-r--r-- directorio
```
Para un archivo
```
 -rw-r--r-- archivo.txt
 ```

### Diferencia de permisos entre archivos y directorios
|Permiso|Archivo|Directorio|
|---|---|---|
|r|Permite abrir y leer un archivo.| Permite listar el contenido de un directorio solo si el permiso de ejecución (x) también está activo.|
|w|Permite escribir en un archivo; sin embargo, este atributo no permite cambiar el nombre de los archivos o eliminarlos. La capacidad de eliminar o cambiar el nombre de los archivos está determinado por los atributos del directorio.|Permite que los archivos dentro de un directorio sean creados, eliminados y renombrados si también se establece el atributo de ejecución. |
|x|Permite que un archivo sea tratado como un programa y pueda ser ejecutado.|Permite entrar al directorio.|

### Modo Octal

|rwx|Binaria|Octal|
|---|---|-|
|rwx|111|7|
|rw-|110|6|
|r-x|101|5|
|r--|100|4|
|-wx|011|3|
|-w-|010|2|
|--x|001|1|
|---|000|0|

### Modo simbólico: Asignar permisos diferentes posibles usuarios.
- `u`: Solo para el usuario.
- `g`: Solo para el grupo.
- `o`: Solo para otros (world).
- `a`: Aplica para todos.


### Lecturas recomendadas
- [Linux permission generator](https://josenoriegaa.github.io/linux-file-system-permission-generator/index.html)

## Modificar Permisos
### Octal
Para proveer todos los permisos al usuario se especifica `7` (equivale a `rwx` o `111` en binario) en la posición de `user`
```
chmod 755 archivo.txt
```
### Tipo de usuario
Quitar (`-`) permisos de lectura (`r`) al usuario (`u`)
```
chmod u-r archivo.txt
```
Volver a dar (`+`) permisos de lectura (`r`) al usuario (`u`)
```
chmod u+r archivo.txt
```
Quitar (-) permisos de ejecución (`x`) al usuario (`u`), y sobre escribir (`=`) el grupo (`g`) y world (`o`, de others) con sólo permisos de escritura (`w`)
```
chmod u-x,go=w mitexto.txt
```
### Revisar cuenta en la que se encuentra
Quién soy?
```
whoami
```
`uid` del usuario e info de grupos a los que pertenece
```
id
```
### Cambiar de usuario
Cambiar a otro usuario (switch user)
```
su <user>
```
Cambiar temporalmente a `root`
```
sudo
```
### Cambiar contraseña
```
passwd
```

## Variables de Entorno
### Crear link simbólico (acceso directo)
```
ln -s ~/terminal_app/app/ Application
```
### Manipular variables de entorno
Mostar todas las variables de entorno que se encuentran configuradas
```
printenv
```
Imprimir la variable de entorno `HOME`
```
echo $HOME
```
Enviar al directorio home
```
cd $HOME
```
Mostrar todas las rutas de los binarios que ejecuta nuestro sistema
```
echo $PATH
```
### Mantener la configuración de las variables de entorno
En `HOME`, existe un archivo que se llama `.bashrc` que es donde está la configuración del Bash. 
```
.bashrc
```
La configuración del `zsh` (utilizado en equipos MAC)
```
.zshrc
```
Se pueden modificar los archivos utilizando, por ejemplo, Visual Studio code
```
code .bashrc
```
## Comandos de Búsqueda
### Encontrar la ruta de los binarios
por ejemplo para `less`
```
which less
```
se obtiene
```
/usr/bin/less
```
### Busqueda de archivos y directorios
Buscar un archivo con nombre `file` en un directorio `./` (y los directorios que éste contiene)
```
find ./ -name file
```
Buscar los archivos terminados en `.txt` utilizando wildcards
```
find ./ -name *.txt
```
Buscar archivos (`f` de files) o directorios (`d` de directories) en `./`
```
find ./ -type fd
```
Encontrar archivos de log
```
find ./ -type f -name *.log
```
Encontrar archivos que sean mayores a 20M
```
find ./ -size 20M
```
### Lecturas recomendadas
- [Bash VS Zsh: Differences and Comparison](https://linuxhint.com/differences_between_bash_zsh/)

## GREP
Encuentra coincidencias de una búsqueda dentro de un archivo de texto.

Por ejemplo, para encontrar todas las películas que tenga la palabra `Towers` dentro de `movies.csv`
```
grep Towers movies.csv
```
obteniendo como respuesta
<pre>
108583,Fawlty <b>Towers</b> (1975,Comedy,-1980,1,54
5952,"Lord of the Rings: The Two <b>Towers</b>, The",Adventure|Fantasy,2002,4,81
</pre>

### Ignorar case sensitive
Para ignorar case sensitive se incluye `-i`, por ejemplo buscando `the` y que incluya también `The`
```
grep -i the movies.csv
```
### Facilitar lectura
Redirigiendo a `less` se facilita la lectura
```
grep -i the movies.csv | less
```
### Contar ocurrencia de la palabra
Para contar las ocurrencias de la palabra se incluye `-c`
```
grep -c the movies.csv
```
para que no se tenga en cuenta el case sensitive se incluye `i` para `-ci`  
```
grep -ci the movies.csv
```
### Encontrar NO coincidencias
Se incluye `-v` para ignorar `tower`
```
grep -vi tower movies.csv
```
### Contar palabras
Para contar todas la palabras en un archivo
```
wc movies.csv
```
obteniendo
```
9126  30006 477779 movies.csv
```
donde 
- `9126` es el número de líneas
- `30006` es el número de palabras
- `477779` es el número de bytes

## Utilidades de red
Revisar información de la red
```
ifconfig
```
Revisar si IP está activa
```
ping www.google.com
```
Traer archivo como texto
```
curl www.google.com
```
Traer un archivo desde internet y descargarlo
```
wget www.google.com
```
Obtener la ruta hasta llegar al sitio que se está visitando
```
traceroute www.google.com
```
Imprimir las conexiones de red, tablas de enrutado, estadísticas, etc.
```
netstat -i
```
## Comprimir archivos
### Comprimir con tar
Se incluye la opción `c` (create) para crear un nuevo archivo comprimido `toCompress.tar` y `v` (verbose) para listar los ficheros procesados detalladamente y `f` (file) para utilizar el archivo `toCompress`
```
tar -cvf toCompress.tar toCompress
```
Se incluye `z` para comprimirlo en formato de compresión gzip
```
tar -cvzf toCompress.tar.gz toCompress
```
### Descomprimir con tar
Se incluye `x` (extract) en vez de `c`
```
tar -xvzf toCompress.tar.gz
```
### Comprimir con zip
Se puede comprimir con zip cuando no es necesario una compresión muy buena, ya que es más simple.

Se utiliza `r` para comprimir de manera recursiva
```
zip -r toCompressInZip.zip toCompress
```
### Descomprimir con unzip
```
unzip toCompressInZip.zip
```
## Manejar procesos
### Ver procesos
Reporta una instantánea de los procesos que están corriendo
```
ps
```
### Matar un proceso
Si se corre un proceso
```
cat & ls
```
se obtiene
<pre>
    PID TTY          TIME CMD
  13951 pts/1    00:00:00 bash
  <b>33131</b> pts/1    00:00:00 cat
  33148 pts/1    00:00:00 ps
</pre>
se puede matar el proceso con
```
kill 33131
```
### Listar procesos que utilizan más recurso
```
top
```
se obtiene
```
    PID USUARIO   PR  NI    VIRT    RES    SHR S  %CPU  %MEM     HORA+ ORDEN    
  13764 luisca1+  20   0   36,9g 135268  65788 S   5,6   3,5   1:50.66 code        1624 luisca1+  20   0  873172  38580  15376 S   5,3   1,0   9:58.74 Xorg     
  13806 luisca1+  20   0  541540  93064  50848 S   4,0   2,4   7:16.69 code     
  13823 luisca1+  20   0   54,7g 314896  89112 S   4,0   8,2  17:30.32 code     
   1782 luisca1+  20   0 4231600 192776  44396 S   2,3   5,0  18:09.71 gnome-s+ 
  13395 luisca1+  20   0   36,4g 222760 108716 S   1,7   5,8  12:49.57 chrome   
    180 root     -51   0       0      0      0 S   1,0   0,0   1:34.41 irq/60-+ 
    655 root      20   0  282392   6116   5276 S   0,3   0,2   1:31.82 thermald 
   1882 luisca1+  20   0  478168   8996   7528 S   0,3   0,2   0:03.31 gsd-sha+ 
  13855 luisca1+  20   0   36,5g  75488  22960 S   0,3   2,0   0:18.65 code     
  13890 luisca1+  20   0   36,4g  44540  14524 S   0,3   1,2   0:55.75 code     
  25086 luisca1+  20   0   36,5g 126492  58732 S   0,3   3,3   2:39.88 code     
  32629 root      20   0       0      0      0 I   0,3   0,0   0:02.18 kworker+ 
```
### Opciones mejoradas htop y glances
Versión mejorada
```
htop
```
Aún mejor, aunque consume más recursos
```
glances
```

## Procesos en Foreground y Background
### Utilizando `ctrl + z`
Mientras que `ctrl + c` finaliza o mata un proceso, `ctrl + z` pausa o suspende un proceso, por lo que después lo podremos volver a llamar con el comando `fg` (foreground, primer plano) o con el comando `bg` (background, segundo plano)

Ejemplo.
Utilizamos `cat` para editar `mi_nota.txt`
```
cat > mi_nota.txt
```
El prompt esperará a que escribamos un texto (se puede escribir y finalizar con `ctrl + d` para guardar).

Sin embargo, suspenderemos con `ctrl + z`.

Obteniendo
```
^Z
[2]+  Detenido     cat > mi_nota.txt
```
Para consultar los procesos en background utilizamos
```
jobs
```
obteniendo (ya había otro proceso `cat`adicional corriendo previamente)
```
[1]-  Detenido   cat
[2]+  Detenido   cat > mi_nota.txt
```
El número a la izquierda es el plano donde está trabajando.

Se le puede llamar con
```
fg 2
```
o para ZSH
```
fg %2
```
retomándolo en primer plano (foreground)
```
cat > mi_nota.txt

```
Se puede matar con `ctrl + c` o guardar el texto con `ctrl + d`.

### Utilizando `&` al final del comando
El operador `&` al final envía un proceso al background
```
cat > mi_nota.txt &
```
obteniendo
```
[1] 49934
```
donde `[1]` es la capa donde se ejecuta y `49934` es el pid.

### Utilizando `bg`
Si se utiliza 
```
bg 1
```
Se le indicará al proceso de la capa 1 que corra (y no se mantenga detenido)

## Editar texto con vim
### Ingresar
```
vim index2.html
```
### Salir
```
:q
```
### Editar texto
Una vez se haya ingresado se ingresa `i` (insertar) para comenzar a editar
```
i
```
para dejar de editar se presiona la tecla `esc`.

### Buscar dentro de un texto
Se debe asegurar que no se esté insertando un texto (presionando `esc`) y utilizando `/<texto a buscar>` se puede buscar un texto
```
/texto_buscado
```
### Borrar
Para borrar una línea se presiona 2 veces la tecla `d`.

### Guardar
```
:w
```
para guardar y salir se ingresa
```
:wq
```
### Lecturas recomendadas
- [Editor nano](https://www.nano-editor.org/)
- [Editor vim](https://www.vim.org/docs.php)
- [GNU Emacs Documentation & Support](https://www.gnu.org/software/emacs/documentation.html)
- [Vim Syntax Highlighting](https://linuxhint.com/vim_syntax_highlighting/)

## Personalizar el terminal
### Instalar
```
sudo apt-get install pacman
```
```
sudo apt-get install tilix
```
### Lecturas recomendadas
- [Tilix](https://gnunn1.github.io/tilix-web/)
- [iTerm para Mac](https://iterm2.com/)
- [Oh my zsh](https://ohmyz.sh/)
- [Power level 10k](https://github.com/romkatv/powerlevel10k)
- [Personaliza tu terminal de cero a cien con Oh My ZSH y Powerlevel10k](https://www.edevars.com/blog/personalizar-terminal)

## Lecturas recomendas
- [Resumen](https://static.platzi.com/media/public/uploads/command-line-cheat-sheet_9d5f3e86-d2c1-4d6e-a921-19aff0dc25e7.pdf)
- [Linux basics for hackers](https://nostarch.com/linuxbasicsforhackers)
- [The linux command line](https://nostarch.com/tlcl2)
- [GREP pocket reference](https://www.oreilly.com/library/view/grep-pocket-reference/9780596157005/)
- [Learning the vi and vim editors](https://www.oreilly.com/library/view/learning-the-vi/9780596529833/)
- [Linux pocket guide](https://www.oreilly.com/library/view/linux-pocket-guide/9781491927557/)
- [Regular Expression Pocket Reference](https://www.oreilly.com/library/view/regular-expression-pocket/9780596514273/)